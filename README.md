# api-request-logs

Use below 2 middlewares in whichever services to log all incoming requests :

install :
    sudo npm i git+https://bitbucket.org/storekingdev/api-request-logs.git --save
    sudo npm i express-device --save

Require:
    var apiRequestLogs = require("api-request-logs").requestLogs;
    var device = require('express-device');

Use below two lines in middleware :
    app.use(device.capture());
    app.use(apiRequestLogs);

    