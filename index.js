var cuti = require("cuti");
var Mongoose = require("mongoose");
var SMCrud = require("swagger-mongoose-crud");
var log4js = cuti.logger.getLogger;
var _ = require("lodash");
var logger = log4js.getLogger("mobileGateway");
let definition = require("./requestLogs.model").definition;
var requestResponseSchema = new Mongoose.Schema(definition);
var requestResponseLoggerCrudder = new SMCrud(requestResponseSchema, "apiRequestLogs", logger);

function createRequestLog(info) {
    return new Promise((resolve, reject) => {
        requestResponseLoggerCrudder.model.create(info, function (err, document) {
            if (err) {
                logger.error(err);
                reject(err);
            } else {
                logger.info("Request is Saved Successfully");
                resolve(document);
            }
        })
    })
}

function updateRequestLog(id, updateData) {
    requestResponseLoggerCrudder.model.findOneAndUpdate({ "_id": id }, { $set: updateData }, function (err, doc) {
        if (err) logger.error(err);
        else logger.info("Log Request is updated Successfully");
    });
}

function requestLogs(req, res, next) {
    if (req.originalUrl == "/api/user/v1/login"
        || req.originalUrl == "/api/user/v1/verifyLoginOtp"
        || req.originalUrl == "/api/user/v1/resetPassword"
        || req.originalUrl == "/api/user/v1/refreshToken"
        || req.originalUrl == "/api/user/v1/forgotPassword"
        || req.originalUrl == "/api/user/v1/verifyforgotpasswordOtp"
        || req.originalUrl == "/api/user/v1/resendforgotpasswordOtp"
        || req.originalUrl == "/api/user/v1/getToken"
        || req.originalUrl == "/api/user/v1/setToken"
        || req.originalUrl == "/api/user/v1/resendOtp"
        || req.originalUrl == "/api/franchise/v1/rfMobileNumberChange"
        || req.originalUrl == "/api/franchise/v1/verify/verifyOtpForRFMobileNumberChange"
        || (req.method == "GET" && /\/user\/v1\/accesspin/g.test(req.url))) {
        next();
    } else {
        let start_time = new Date().getTime();
        let data = {};
        data.version = req.get('X-ClientVersion');
        data.platformVersion = req.get('X-ClientPlatformVersion');
        data.device = req.get('X-ClientDevice');
        data.locale = req.get('X-ClientLocale');
        data.method = req.method;
        data.originalUrl = req.originalUrl;
        data.headers = _.assign({}, req["headers"]);
        data.requestedUser = req.user && req.user.username ? req.user.username : "";

        /* if (data.headers && data.headers.authorization) {
            delete data.headers.authorization
        }
        if (req.headers && req.headers.callerip) {
            data.requestIp = _.replace(req.headers.callerip, "::ffff:", "");
        } */

        data.path = req.path;
        data.queryParams = req.query ? req.query : {};
        data.body = req.body ? req.body : {};
        data.respose = "";
        data.logSource = req["logSource"];
        data.platform = req.device && req.device.type? req.device.type : "";

        createRequestLog(data).then(savedData => {
            let requestId = savedData._id;
            let updateData = {};

            var oldWrite = res.write,
                oldEnd = res.end;

            var chunks = [];

            res.write = function (chunk) {
                chunks.push(chunk);
                oldWrite.apply(res, arguments);
            };

            res.end = function (chunk) {
                if (chunk)
                    chunks.push(chunk);

                try {
                    var body = Buffer.concat(chunks).toString('utf8');
                    updateData.responseSize = Buffer.byteLength(body);
                } catch (e) {
                    updateData.responseSize = 0;
                    updateData.response = e;
                }

                oldEnd.apply(res, arguments);
            };

            res.on("finish", function () {
                updateData.responseTime = new Date().getTime() - start_time;
                updateData.responseStatusCode = res.statusCode;

                if (requestId)
                    updateRequestLog(requestId, updateData);
            });

            res.on("error", function (err) {
                updateData.responseTime = new Date().getTime() - start_time;
                updateData.responseStatusCode = res.statusCode;
                updateData.response = err;

                if (requestId)
                    updateRequestLog(requestId, updateData);
            })
            next();
        }).catch(err => {
            next();
        });
    }
    // })
}

function removeOldLogs() {
    return new Promise((resolve, reject) => {
        let date = new Date();
        //date = new Date(date.setMonth(date.getMonth() - 2));
        date = new Date(date.setDate(date.getDate() - 15));
        requestResponseLoggerCrudder.model.remove({ "createdAt": { "$lt": date } }, function (err, doc) {
            if (err) {
                logger.error(err);
                reject();
            } else {
                logger.info(`Logs older than ${date} are removed Successfully`);
                resolve();
            }
        });
    });
}

function getIpAddress(req, res, next) {
    var ip;
    if (req.headers['x-forwarded-for']) {
        ip = req.headers['x-forwarded-for'].split(",")[0];
    } else if (req.connection && req.connection.remoteAddress) {
        ip = req.connection.remoteAddress;
    } else if (req.headers['cf-connecting-ip']) {
        ip = req.headers['cf-connecting-ip'];
    } else {
        ip = req.ip;
    }

    if (req.headers) {
        req.headers["clientIpAddress"] = ip;
    }
    next();
}

module.exports = {
    createRequestLog: createRequestLog,
    updateRequestLog: updateRequestLog,
    requestLogs: requestLogs,
    removeOldLogs: removeOldLogs,
    getIpAddress: getIpAddress
}
