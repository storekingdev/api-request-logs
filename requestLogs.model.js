var Mongoose = require("mongoose"),
    Schema = Mongoose.Schema;
var definition = {
    version: {
        type: String
    },
    platformVersion: {
        type: String
    },
    device: {
        type: String
    },
    locale: {
        type: String
    },
    method: {
        type: String
    },
    originalUrl: {
        type: String
    },
    headers: {
        type: Schema.Types.Mixed
    },
    queryParams: {
        type: Schema.Types.Mixed
    },
    body: {
        type: Schema.Types.Mixed
    },
    responseStatusCode: {
        type: Schema.Types.Mixed
    },
    response: {
        type: Schema.Types.Mixed
    },
    logSource: {
        type: String
    },
    responseTime: {
        type: Schema.Types.Mixed
    },
    path: {
        type: String
    },
    createdAt: {
        type: Date
    },
    responseSize: {
        type: Schema.Types.Mixed//In Bytes
    },
    platform: {
        type: Schema.Types.Mixed
    },
    requestedUser: {
        type: Schema.Types.Mixed
    },
    requestIp: {
        type: String
    }
};

module.exports = {
    definition: definition
}